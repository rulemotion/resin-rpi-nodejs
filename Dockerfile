FROM resin/rpi-buildstep-armv6hf:latest
ONBUILD ADD . /app
ONBUILD RUN /bin/bash -c ". /.env && /build/builder" && rm -rf /cache/app/node_modules && /app/vendor/node/bin/npm cache clear
ONBUILD CMD /start
